use plain_theme::*;
use plain_widgets::input::*;

use piston::window::*;
use piston::input::*;
use piston::event_loop::*;
use graphics::*;
use opengl_graphics::GlGraphics;

use font::*;

pub enum PistonEvent<'a> {
    CursorInput(CursorInput, &'a mut Theme<PistonFont>, [u64; 2]),
    TextInput(String),
    KeyboardInput(bool, Key),
    Render(&'a mut GlGraphics, Viewport, &'a mut Theme<PistonFont>, [u64; 2], Cursor),
    Update(f64),
}

pub struct PistonBackend<W: Window + BuildFromWindowSettings> {
    pub window: W,
    pub g: GlGraphics,
    pub events: Events,
    pub size: [u64; 2],
    pub theme: Theme<PistonFont>,
    pub cursor: Cursor,
    pub zoom: Option<i64>,
}

impl<W: Window + BuildFromWindowSettings> PistonBackend<W> {
    pub fn next<'a>(&'a mut self) -> Option<PistonEvent<'a>> {
        if let Some(zoom) = self.zoom {
            self.zoom = None;
            return Some(PistonEvent::CursorInput(
                CursorInput {
                    state: self.cursor,
                    action: CursorAction::Zoom(zoom),
                },
                &mut self.theme,
                self.size,
            ));
        }
        while let Some(e) = self.events.next(&mut self.window) {
            match e {
                Event::Input(input) => {
                    match input {
                        Input::Move(motion) => {
                            if let Some(action) = match motion {
                                Motion::MouseCursor(x, y) => {
                                    self.cursor.pos = [x as i64, y as i64];
                                    None
                                }
                                Motion::MouseRelative(x, y) => Some(CursorAction::Move(
                                    [x as i64, y as i64],
                                )),
                                Motion::MouseScroll(x, y) => {
                                    self.zoom = Some(y as i64);
                                    Some(CursorAction::Scroll([x as i64, y as i64]))
                                }
                                _ => None,
                            }
                                {
                                    return Some(PistonEvent::CursorInput(
                                        CursorInput {
                                            state: self.cursor,
                                            action,
                                        },
                                        &mut self.theme,
                                        self.size,
                                    ));
                                }
                        }
                        Input::Button(button_args) => {
                            let down = match button_args.state {
                                ButtonState::Press => true,
                                ButtonState::Release => false,
                            };

                            match button_args.button {
                                Button::Mouse(mouse_button) => {
                                    let button = mouse_button as u8;
                                    let button = if button > 0 { button - 1 } else { button };

                                    self.cursor.buttons[button as usize] = down;

                                    return Some(PistonEvent::CursorInput(
                                        CursorInput {
                                            state: self.cursor,
                                            action: CursorAction::Button(button, down),
                                        },
                                        &mut self.theme,
                                        self.size,
                                    ));
                                }
                                Button::Keyboard(key) => {
                                    return Some(PistonEvent::KeyboardInput(down, key));
                                }
                                _ => (),
                            }
                        }
                        Input::Text(text) => {
                            return Some(PistonEvent::TextInput(text));
                        }
                        _ => (),
                    }
                }
                Event::Loop(loop_event) => {
                    match loop_event {
                        Loop::Render(args) => {
                            self.size = [args.width as u64, args.height as u64];
                            return Some(PistonEvent::Render(
                                &mut self.g,
                                args.viewport(),
                                &mut self.theme,
                                self.size,
                                self.cursor,
                            ));
                        }
                        Loop::Update(UpdateArgs { dt }) => {
                            return Some(PistonEvent::Update(dt));
                        }
                        _ => (),
                    }
                }
                _ => (),
            }
        }
        None
    }
}
